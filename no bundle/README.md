#### INSTALL RUST
        $ curl https://sh.rustup.rs -sSf | sh
#### INSTALL WASM
        $ curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
#### BUILD
        $ wasm-pack build --target web